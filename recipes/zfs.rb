#
# Cookbook Name:: cerny
# Recipe:: zfs
#
# Copyright 2014, Cerny.cc
#
# All rights reserved - Do Not Redistribute
#

apt_repository 'zfs-native' do
  uri          'http://ppa.launchpad.net/zfs-native/stable/ubuntu/'
  distribution node['lsb']['codename']
  components   ['main']
  keyserver    'keyserver.ubuntu.com'
  key          'F6B0FC61'
end

package 'ubuntu-zfs'
