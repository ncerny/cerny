#
# Cookbook Name:: cerny
# Recipe:: _data_bag
#
# Copyright (C) 2014 Nathan Cerny
#
# All rights reserved - Do Not Redistribute
#
if node['cerny']
  if node['cerny']['data_bag']
    bag = data_bag_item(node['cerny']['data_bag'], node['fqdn'])
  end
end

bag.each do |k,v|
  node.default[k] = v
end unless bag.nil?
