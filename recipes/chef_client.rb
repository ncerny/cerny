#
# Cookbook Name:: cerny
# Recipe:: chef_client
#
# Copyright (C) 2014 Nathan Cerny
#
# All rights reserved - Do Not Redistribute
#
execute 'import-chef-ssl-cert' do
  command '/usr/bin/knife ssl fetch -c /etc/chef/client.rb'
  not_if '/usr/bin/knife ssl check -c /etc/chef/client.rb'
end

include_recipe 'chef-client::config'
include_recipe 'chef-client'
