#
# Cookbook Name:: cerny
# Recipe:: network
#
# Copyright (C) 2014 Nathan Cerny
#
# All rights reserved - Do Not Redistribute
#
node['cerny']['network'].each do |key,val|
  node.default['udev']['net'][key] = val['hwaddr'] if val['hwaddr']
end

include_recipe 'udev::net'
include_recipe 'network_interfaces'

node['cerny']['network'].each do |key,val|
  package 'ifenslave'
  package 'vlan'

  fe = Chef::Util::FileEdit.new('/etc/modules')
  fe.insert_line_if_no_match('bonding', 'bonding')
  fe.insert_line_if_no_match('8021q', '8021q')
  fe.write_file

  network_interfaces key do
    onboot      val['onboot']     if val['onboot']
    bootproto   val['type']       if val['type']
    target      val['target']     if val['target']
    network     val['network']    if val['network']
    mask        val['mask']       if val['mask']
    gateway     val['gateway']    if val['gateway']
    broadcast   val['broadcast']  if val['broadcast']
    bridge      val['bridge']     if val['bridge']
    bridge_stp  val['bridge_stp'] if val['bridge_stp']
    vlan_dev    val['vlan_dev']   if val['vlan_dev']
    bond        val['bond']       if val['bond']
    bond_mode   val['bond_mode']  if val['bond_mode']
    metric      val['metric']     if val['metric']
    mtu         val['mtu']        if val['mtu']
    pre_up      val['pre_up']     if val['pre_up']
    up          val['up']         if val['up']
    post_up     val['post_up']    if val['post_up']
    pre_down    val['pre_down']   if val['pre_down']
    down        val['down']       if val['down']
    post_down   val['post_down']  if val['post_down']
    custom      val['custom']     if val['custom']
  end
end
