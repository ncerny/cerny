#
# Cookbook Name:: cerny
# Recipe:: default
#
# Copyright (C) 2014 Nathan Cerny
#
# All rights reserved - Do Not Redistribute
#
include_recipe 'apt'
include_recipe 'cerny::_data_bag'
include_recipe 'cerny::network'
include_recipe 'cerny::chef_client'
include_recipe 'cerny::ntp'
