#
# Cookbook Name:: cerny
# Recipe:: ntp
#
# Copyright 2014, Nathan Cerny
#
# All rights reserved - Do Not Redistribute
#
ntpservers = []
ntp_servers = search(:node, 'tags:ntp_server')
ntp_servers.each do |ntp_server|
  ntpservers << ntp_server.fqdn
end

if tagged?('ntp_server')
  node.force_default['ntp']['servers'] = [
    '0.north-america.pool.ntp.org',
    '1.north-america.pool.ntp.org',
    '2.north-america.pool.ntp.org',
    '3.north-america.pool.ntp.org'
  ]
  node.default['ntp']['peers'] = ntpservers
  node.default['ntp']['is_server'] = 'true'
  node.default['ntp']['restrictions'] = [
    '192.168.100.0 255.255.255.0 nomodify notrap',
    '192.168.101.0 255.255.255.0 nomodify notrap',
    '192.168.102.0 255.255.255.0 nomodify notrap',
    '192.168.103.0 255.255.255.0 nomodify notrap'
  ]
else
  node.default['ntp']['servers'] = ntpservers
  node.default['ntp']['is_server'] = 'false'
end

include_recipe 'ntp'
