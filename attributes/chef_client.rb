default['chef-client']['interval']  = '900'
default['chef-client']['log_dir']   = '/var/log/chef'
default['chef-client']['log_file']  = 'chef-client.log'

default['chef-client']['config']['chef_server_url'] = ''
default['chef-client']['config']['ssl_verify_mode'] = ':verify_peer'
